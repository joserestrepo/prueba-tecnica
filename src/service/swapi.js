const https = require('https');
const http = require('http');

function resquestSwapiHttps(url){
    let schema;
    let promesa = new Promise((resolve, rejects) =>{
        var getReq = https.get(url, function(res) {
        let chunks = [];
        res.on('data', function(data) {
            chunks.push(data);
        }).on('end', function(){
            let data   = Buffer.concat(chunks);
            schema = JSON.parse(data);
            resolve(schema.results);
        });
        });
    
        //end the request
        getReq.end();
        getReq.on('error', function(err){
            console.log("Error: ", err);
            rejects(err);
        }); 
    })
    

    return promesa;
}

function resquestSwapiHttp(url){
    let schema;
    let promesa = new Promise((resolve, rejects) =>{
        var getReq = http.get(url, function(res) {
        let chunks = [];
        res.on('data', function(data) {
            chunks.push(data);
        }).on('end', function(){
            let data   = Buffer.concat(chunks);
            schema = JSON.parse(data);
            resolve(schema.results);
        });
        });
    
        //end the request
        getReq.end();
        getReq.on('error', function(err){
            console.log("Error: ", err);
            rejects(err);
        }); 
    })
    

    return promesa;
}

module.exports = {
    resquestSwapiHttps,
    resquestSwapiHttp
}