const { resquestSwapiHttp, resquestSwapiHttps } = require('./service/swapi');

async function runApi() {
  
    const films = await resquestSwapiHttps('https://swapi.dev/api/films/');
    console.log(films)
    films.forEach(items => {
        items.planets.map(async (url) =>{
            let response = await resquestSwapiHttp(url);
            console.log(response);
        })
    });
}

runApi();

